FROM python:3.11-alpine3.18 as python

FROM python as python-build-stage

RUN apk update

COPY requirements.txt /requirements.txt

RUN pip wheel --wheel-dir /usr/src/app/wheels  \
  -r /requirements.txt

FROM python as python-run-stage

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

ARG APP_HOME=/app

WORKDIR ${APP_HOME}

RUN addgroup --system django \
    && adduser --system --ingroup django django

COPY --from=python-build-stage /usr/src/app/wheels  /wheels/

RUN \
    pip install --no-cache-dir --no-index --find-links=/wheels/ /wheels/* \
    && rm -rf /wheels/

COPY --chown=django:django ./start.sh /start.sh
COPY --chown=django:django ./mysite ${APP_HOME}/mysite

ENTRYPOINT ["/start.sh"]

ENV DJANGO_SETTINGS_MODULE=mysite.settings

USER django
