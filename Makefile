SHELL := /bin/bash

PYTHON ?= python3.11
VENVDIR ?= $(abspath ./venv)
DJANGO_MANAGE ?= $(VENVDIR)/bin/python3 manage.py

export PATH := $(VENVDIR)/bin:$(PATH)

setup-venv:
	$(PYTHON) -m venv venv
	$(VENVDIR)/bin/pip install pip-tools

pip-compile:
	$(VENVDIR)/bin/pip-compile -o requirements.txt requirements.in

pip-sync:
	$(VENVDIR)/bin/pip-sync requirements.txt

pip-compile-sync: pip-compile pip-sync
