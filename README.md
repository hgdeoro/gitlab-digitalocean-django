Let's iteratively deploy a Django app to Digital Ocean. Baby steps. Be agile ;)

# v0.1

Goal: have the simplest Django project deployed and running (ignoring good practices and security issues).

* Based on https://docs.djangoproject.com/en/4.2/intro/tutorial01/
* Toy application:
  * Only a Django project, without any app.
  * Local SQLite database
  * Runs using `manage.py runserver`
* Build a simple Docker image
* Insecure! (`SECRET_KEY`, `ALLOWED_HOSTS=*`, etc.)
* Can be deployed (manually) to the DigitalOcean App Platform
